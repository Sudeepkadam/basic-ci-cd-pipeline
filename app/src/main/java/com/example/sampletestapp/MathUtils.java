package com.example.sampletestapp;

public class MathUtils {

    public boolean areEqual(int numberOne, int numberTwo) {
        return numberOne == numberTwo;
    }

    public int addTwoNumbers(int numberOne, int numberTwo) {
        return numberOne + numberTwo;
    }

    public boolean isEven(int number) {
        return number%2 == 0;
    }

    public boolean isPrime(int number) {
        if (number < 2) {
            return false;
        }

        for (int i = 2; i <= (number/2); i++) {
            if (number%i == 0) {
                return false;
            }
        }
        return true;
    }

    public int greaterNumber(int numberOne, int numberTwo) {
        return Math.max(numberOne, numberTwo);
    }
}
