package com.example.sampletestapp;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.*;

import android.content.Context;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.sampletestapp", appContext.getPackageName());
    }

    @Test
    public void verifyHomeScreen() {
        onView(withId(R.id.button_to_first)).check(matches(isDisplayed()));
        onView(withId(R.id.button_to_second)).check(matches(isDisplayed()));
        onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));
        onView(withText(R.string.home_fragment)).check(matches(isDisplayed()));
    }

    @Test
    public void verifyFirstScreen() {
        onView(withId(R.id.button_to_first)).check(matches(isDisplayed())).perform(ViewActions.click());
        onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));
        onView(withText(R.string.first_fragment)).check(matches(isDisplayed()));
    }

    @Test
    public void verifySecondScreen() {
        onView(withId(R.id.button_to_second)).check(matches(isDisplayed())).perform(ViewActions.click());
        onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));
        onView(withText(R.string.second_fragment)).check(matches(isDisplayed()));
    }

}