package com.example.sampletestapp;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import androidx.annotation.NonNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    MathUtils mathUtils;

    @Before
    public void setup() {
        mathUtils = new MathUtils();
    }

    @Test
    public void addition_isCorrect() {
        assertEquals(4, mathUtils.addTwoNumbers(2, 2));
    }

    @Test
    public void verifyIsEven() {
        assertTrue(mathUtils.isEven(10));
        assertFalse(mathUtils.isEven(9));
    }

    @Test
    public void verifyGreaterNum() {
        assertEquals(11, mathUtils.greaterNumber(10, 11));
        assertEquals(9, mathUtils.greaterNumber(9, 8));
    }

    @Test
    public void verifyEqual() {
        assertTrue(mathUtils.areEqual(10, 10));
        assertFalse(mathUtils.areEqual(9, 8));
    }

    @Test
    public void verifyIsPrime() {
        assertTrue(mathUtils.isPrime(5));
        assertFalse(mathUtils.isPrime(10));
    }
}